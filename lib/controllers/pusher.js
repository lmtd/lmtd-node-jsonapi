var Pusher = require('./../services/pusher.js');

module.exports.put = function(channel, data) {

    Pusher.trigger(channel, 'put', data);

    return Promise.resolve(true);

};

module.exports.delete = function(channel, data) {

    Pusher.trigger(channel, 'delete', data);

    return Promise.resolve(true);

};

module.exports.post = function(channel, data) {

    Pusher.trigger(channel, 'post', data);

    return Promise.resolve(true);

};
