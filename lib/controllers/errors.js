var HTTPStatus = require('http-status');
var Logger = require('.././services/logger.js');
//

module.exports.UNAUTHORIZED = function(responde, detail) {

	send( responde, HTTPStatus.UNAUTHORIZED, details(detail, HTTPStatus[HTTPStatus.UNAUTHORIZED] ) );

};

//

module.exports.BAD_REQUEST = function(responde, detail) {

	send( responde, HTTPStatus.BAD_REQUEST, details(detail, HTTPStatus[HTTPStatus.BAD_REQUEST] ) );

};

//

module.exports.NOT_FOUND = function(responde, detail) {

	send( responde, HTTPStatus.NOT_FOUND, details(detail, HTTPStatus[HTTPStatus.NOT_FOUND] ) );

};

//

module.exports.INTERNAL_SERVER_ERROR = function(responde, detail) {

	send( responde, HTTPStatus.INTERNAL_SERVER_ERROR, details(detail, HTTPStatus[HTTPStatus.INTERNAL_SERVER_ERROR] ) );

	if ( detail.stack ) {
		Logger.error(detail.stack);
	} else {
		Logger.error(JSON.stringify(detail));
	}

};

//

function details(detail, code) {

	//if ( typeof(detail) === 'object' ) {
	//	detail = String(detail);
	//}

	return { error: { status: code, details: detail } };

	/*
	if ( detail ) {
		if ( typeof(detail) === 'string' ) {
			return { error: { status: code, details: detail } };
		} else {
			if ( String(detail) === '[object Object]' ) {
				return { error: { status: code, details: JSON.stringify(detail) } };
			} else {
				return { error: { status: code, details: String(detail) } };
			}
		}
	} else {
		return { error: { status: code } };
	}
	*/
}

function send(responde, status, error) {

	responde.status(status);
	responde.json(error);

}
