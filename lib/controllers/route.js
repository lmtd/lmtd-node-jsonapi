'use strict';
//
var ExpressInitializer = require('./../initializers/express.js');
var express = ExpressInitializer.express;
var app = ExpressInitializer.app;

var Serlializer =  require('./serializer.js');
//

var Errors = require('./../controllers/errors.js');
var Deserializer = require('./../controllers/deserializer.js');

//

// INIT ------------------------------------------------------------------------

module.exports.init = function(path, deserializer, json) {

	if ( deserializer === undefined ) {
		deserializer = true;
	}

	if ( json === undefined ) {
		json = true;
	}

	//

	var router = express.Router();

	if ( json === true ) {
		router.use(ExpressInitializer.bodyParser.json({ type: 'application/vnd.api+json' }));
	} else {
		router.use(ExpressInitializer.bodyParser.json());
	}

	if ( deserializer === true ) {
		router.use( Deserializer.middleware );
	}

	app.use(path, router);

	//

	return router;

};

// COUNT

module.exports.count = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	router.route('/count').get(function(request, response) {

		function transform(data) {

			return Promise.resolve({ count: data });

		}

		function operation() {

			return Model.promiseCount();

		}

		task( operation, request, response, options.authentication, options.pretransform, transform);

	});

};

// GET LIST --------------------------------------------------------------------

function transformList(data, request) {

	const result = {};
	result.data = [];

	// BASIC SERALIZATION ------------------------------------------------------

	for ( let i=0; i < data.length; i += 1 ) {
		result.data.push( data[i].serialize() );
	}

	// IF INCLUDED -------------------------------------------------------------

	if ( request.query.included ) {

		// GET THE FULL QUERY LIST
		const includedList = request.query.included.split(',');

		let queries = {};

		for ( let i=0; i < data.length; i += 1 ) {
			queries = data[i].getIncludedQueries(queries, includedList);
		}

		// GET ALL RELATED DATA
		return Serlializer.getIncludedData(queries).then(function(includedData) {

				result.included = includedData.list;

				// CLEAN ORIGINAL OBJECTS
				const ids = includedData.ids;

				for ( let i=0; i < result.data.length; i += 1 ) {

					result.data[i] = Serlializer.clean(result.data[i], ids, includedList);

				}

			return Promise.resolve(result);

		});

	} else {

		return Promise.resolve(result);

	}

}



module.exports.list = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	if ( Model.schema.paths.position && !options.sort ) {
		options.sort = { position: 1 };
	}

	router.route('/').get( function(request, response) {

		var query = {};
		if ( options.query ) {
			query = options.query;
		}

		var operation = {};
		if ( options.operation ) {
			operation = options.operation;
		}

		if ( request.query ) {

			// FILTERING
			Object.keys(request.query).forEach( function(param) {
				if ( param !== 'included' && param !== 'limit' && param !== 'skip' ) {
					query[param] = { '$regex': request.query[param], '$options': 'i' };
				}
			});

			//
			if ( request.query.limit ) {
				operation.limit = request.query.limit;
				if ( operation.limit > 500 ) {
					operation.limit = 500;
				}
				operation.skip = 0;
			}
			if ( request.query.skip ) {
				operation.skip = request.query.skip;
			}

		}

		function operationFunction() {

			if ( options && options.list === false ) {
				if ( operation.limit ) {
					return Model.getList(query, options.fields, operation);
				} else {
					return Promise.reject({ message: 'limit parameter is missing'});
				}
			} else {
				return Model.getList(query, options.fields, operation);
			}

		}

		task( operationFunction, request, response, options.authentication, options.before, options.pretransform, transformList, options.transform, options.after );

	});

};

// GET ------------------------------------------------------------------------------

function transformGet(data, request) {

	// BASIC SERIALIZATION
	var result = {};
	result.data = data.serialize();

	// IF INCLUDED -------------------------------------------------------------

	if ( request.query.included ) {

		// GET THE FULL QUERY LIST
		var includedList = request.query.included.split(',');

		let queries = {};

		queries = data.getIncludedQueries(queries, includedList);

		// GET ALL RELATED DATA
		return Serlializer.getIncludedData(queries).then(function(includedData) {
			
			// SAVE RELATIONSHIPS
			result.included = includedData.list;

			// CLEAN ORIGINAL OBJECTS
			const ids = includedData.ids;

			result.data = Serlializer.clean(result.data, ids, includedList);

			return Promise.resolve(result);

		});

	} else {
		return Promise.resolve(result);
	}

}

module.exports.get = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	router.route('/:id').get( function(request, response) {

		function operation() {
			return Model.getOneById(request.params.id);
		}

		task( operation, request, response, options.authentication, options.before, options.pretransform, transformGet, options.transform, options.after );

	});

};

// POST ------------------------------------------------------------------------------

module.exports.post = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	router.route('/').post( function(request, response) {

		function transform(data) {
			return Promise.resolve({ data: data.serialize() });
		}

		function operation(data, request, response, user) {

			delete request.body.created_at;
			delete request.body.modified_at;

			var document = new Model(request.body);

			if ( options ) {
				if ( options.created_by === true ) {
					document.created_by = user.id;
				}
			}

			return document.promiseSave();

		}

		task( operation, request, response, options.authentication, options.before, options.pretransform, transform, options.transform, options.after );

	});

};

// PUT ------------------------------------------------------------------------------

module.exports.put = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	router.route('/:id').patch( function(request, response) {

		function transform(data) {
			return Promise.resolve({ data: data.serialize() });
		}

		function operation(data, request, response, user) {

			return Model.getOneById(request.params.id).then(function(document) {

				delete request.body.created_at;
				delete request.body.modified_at;

				if ( options ) {
					if ( options.created_by === true ) {
						request.body.modified_by = user.id;
					}
				}

				return document.update(request.body).then(function() {
					return Promise.resolve(document);
				});

			});

		}

		task( operation, request, response, options.authentication, options.before, options.pretransform, transform, options.transform, options.after );

	});

};

// DELETE ------------------------------------------------------------------------------

module.exports.delete = function(router, Model, options) {

	if ( !options ) {
		options = {};
	}

	router.route('/:id').delete( function(request, response) {

		function transform(data) {
			return Promise.resolve({ data: { id: data.id, type: data.type } } );
		}

		function operation() {
			return Model.removeById(request.params.id);
		}

		task( operation, request, response, options.authentication, options.before, options.pretransform, transform, options.transform, options.after );

	});

};

// PRIVATE ------------------------------------------------------------------------------

function task(operation, request, response, authentication, before, pretransform, transform, optional, after) {

	var user = {};

	return process(authentication, null, request, response)

	.then(function(data) {
		user = data;
		return process(operation, data, request, response, user);
	})

	.then(function(data) {
		return process(pretransform, data, request, response, user);
	})

	.then(function(data) {
		return process(transform, data, request, response, user);
	})

	.then(function(data) {
		return process(before, data, request, response, user);
	})

	.then(function(data) {
		return process(optional, data, request, response, user);
	})

	.then(function(data) {

		response.json(data);

		return process(after, data, request, response, user).then(function(data) {

		}).catch(function(error) {

		});

	})

	.catch(function(error) {
		if ( error ) {
			if ( error.name ) {
				if ( error.name === 'authentication' ) {
					Errors.UNAUTHORIZED(response, error.message);
				} else {
					Errors.INTERNAL_SERVER_ERROR(response, error);
				}
			} else {
				Errors.INTERNAL_SERVER_ERROR(response, error);
			}
		} else {
			Errors.NOT_FOUND(response);
		}
	});

}

function process(method, data, request, response, user) {
	if ( method ) {
		return method(data, request, response, user);
	} else {
		return Promise.resolve(data);
	}
}
