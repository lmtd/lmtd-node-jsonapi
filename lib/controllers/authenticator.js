var options = { id: 'id', email: 'email', password: 'password', token: 'token' };

// LOGIN

module.exports.options = options;

module.exports.login = function(request, response, Model) {

	return new Promise(function(resolve, reject) {

		var criteria;
		if ( request.body[options.id] ) {
			criteria = { _id: request.body[options.id] };
		} else if ( request.body[options.email] ) {
			criteria = { email: request.body[options.email] };
		}

		if ( criteria && request.body[options.password] ) {
			Model.getOneByParameter(criteria).then(function(document) {
				if ( document.authenticate( request.body[options.password] ) ) {
					var result = {};
					result[options.token] = document.token;
					result[options.id] = document.id;
					resolve(result);
				} else {
					reject('password');
				}
			}).catch(function(error) {
				reject('email');
			});
		} else {
			reject('request');
		}

	});

};

// AUTHENTICATION

module.exports.authentication = function(data, request, response, Model, params) {
	return new Promise(function(resolve, reject) {
		var headers = request.headers;
		if ( params ) {
			headers = params;
		}
		if ( headers && headers[options.token] && headers[options.id] ) {
			Model.getOneById(headers[options.id]).then(function(document) {
				if ( document.token === headers[options.token] ) {
					resolve(document);
				} else {
					reject({ name: 'authentication', message: 'Invalid token'});
				}
			}).catch(function() {
				reject({ name: 'authentication', message: 'Invalid ID'});
			});
		} else {
			reject({ name: 'authentication', message: 'Invalid headers'});
		}
	});
};

//

module.exports.dependant = function(data, request, response, Model, parameter, parameters) {

	var self = this;

	return new Promise(function(resolve, reject) {

		self.authentication(data, request, response, Model, parameters).then(function(document) {

			if ( document[parameter].indexOf(request.params.id) !== -1 ) {
				resolve(document);
			} else {
				reject({ name: 'authentication', message: 'Object and User'});
			}

		}).catch(function(error) {
			reject(error);
		});

	});

};

//
