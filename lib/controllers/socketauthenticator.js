var SocketService = require('./../services/socket.js');

module.exports.init = function(Authenticator) {

    SocketService.listeners(function(socket) {

        socket.on('authenticate', function(data) {

            Authenticator.authentication(null, { headers: data }, null).then(function(user) {
                socket.join(user._id);
                socket.emit('authenticated');
            });

        });

    });

};

// UPDATE

module.exports.put = function(data, request, response, user) {
    var id; if ( user ) { id = user._id; }
    return new Promise(function(resolve) {
        SocketService.put(data, id);
        resolve(data);
    });
};

// POST

module.exports.post = function(data, request, response, user) {
    var id; if ( user ) { id = user._id; }
    return new Promise(function(resolve) {
        SocketService.post(data, user._id);
        resolve(data);
    });
};

// DELETE

module.exports.delete = function(data, request, response, user) {
    var id; if ( user ) { id = user._id; }
    return new Promise(function(resolve) {
        SocketService.delete(data, user._id);
        resolve(data);
    });
};
