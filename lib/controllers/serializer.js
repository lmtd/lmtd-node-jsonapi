'use strict';

var mongoose = require('mongoose');

// CLEAN -----------------------------------------------------------------------

module.exports.clean = function(object, ids, queryList) {

    for ( let param in object.relationships ) {

        if ( queryList.indexOf(param) !== -1 ) {

            const list = object.relationships[param].data;

            if ( Array.isArray(list) ) {

                for ( let i=0; i < list.length; i += 1 ) {

                    const id = list[i].id;
                    if ( ids.indexOf(String(id)) === -1 ) {

                        object.relationships[param].data.splice(i, 1);
        			}

                }

            } else {

                const id = list.id;
                if ( ids.indexOf(String(id)) === -1 ) {
                    delete object.relationships[param];
                }

            }

        }

	}

    return object;

};



// GET INCLUDED DATA -----------------------------------------------------------

module.exports.getIncludedData = function(queries) {

	const promises = [];

	Object.keys(queries).forEach(function(param) {
		const Model = mongoose.model(param);
		const list = queries[param];
		const promise = Model.getList( { _id: { $in: list } });
		promises.push(promise);
	});


	if ( promises.length > 0 ) {

		return Promise.all(promises).then(function(result) {

			const array = [];
            const ids = [];

			for ( let i=0; i < result.length; i += 1 ) {

				const list = result[i];

				for ( let n=0; n < list.length; n += 1 ) {

                    ids.push(list[n].id);
					array.push(list[n].serialize());

				}

			}

			return Promise.resolve({ list: array, ids:ids });

		});
	} else {
		return Promise.resolve({ list: [], ids:[] });
	}

};
