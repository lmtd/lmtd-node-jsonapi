var Resizer = require('.././services/resizer.js');
var S3 = require('.././services/amazons3.js');

//

var sizes = [160, 320, 480, 640, 960, 1280, 1920, 2560];

// FILE ------------------------------------------------------------------------

module.exports.file = function(Model, result) {

    return createFile(Model, result, true).then(function(document) {

        return S3.transfer(result.path, document.filename, document.mimetype).then(function() {

            return Promise.resolve(document.serialize());

        });

    });

};

// IMAGE -----------------------------------------------------------------------

module.exports.image = function(Model, result) {

    return createImage(Model, result).then(function(document) {

        return createSizes(Model, result.path, document).then(function() {

            return S3.transfer(result.path, document.filename, document.mimetype).then(function() {

                return Promise.resolve(document.serialize());

            });

        });

    });

};

function createImage(Model, file) {

	return createFile(Model, file, false).then(function(document) {

        return Resizer.getSize(file.path).then(function(size) {

    		document.width = size.width;
    		document.height = size.height;
    		document.sizes = Model.calculateSize(document.width);

            return document.promiseSave();

    	});

    });

}

function createSize(document, Model, path, width) {

	var newpath = path+'_'+width;

	return Resizer.resize(path, newpath, width).then(function() {

		return S3.transfer(newpath, Model.calculateFilename(document.id, width, document.extension), document.mimetype);

	});

}

function createSizes(Model, path, document) {

	var promises = [];
	for ( var i=0; i < document.sizes; i += 1 ) {

		var promise = createSize(document, Model, path, sizes[i]);
		promises.push(promise);
	}
	return Promise.all(promises);

}

// INTERNALS -------------------------------------------------------------------

function createFile(Model, file, save) {
	var document = new Model({
		size: file.size,
		originalname: file.originalname,
		mimetype: file.mimetype
	});
    if ( save ) {
        return document.promiseSave();
    } else {
        return Promise.resolve(document);
    }
}
