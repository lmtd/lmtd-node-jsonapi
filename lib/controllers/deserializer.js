'use strict';

var Errors = require('./../controllers/errors.js');

// DESERILAIZE

module.exports.deserialize = function(object) { // FROM WORLD TO MONGO

	const data = {};
	object = object.data; // DATA IS WRAPPED IN DATA
	if ( object.id ) {
		data.id = object.id;
	}

	// ATTRIBUTES
	if ( object.attributes ) {
		Object.keys(object.attributes).forEach(function(param) {
			data[param] = object.attributes[param];
		});
	}

	// RELATIONSHIPS
	if ( object.relationships ) {
		Object.keys(object.relationships).forEach(function (param) {
			if ( object.relationships[param].data ) {
				if ( object.relationships[param].data.length ) { // IF ARRAY
					data[param] = []; // CREATE EMPTY ARRAY
				 	const list = object.relationships[param].data;
					for ( let i=0; i < list.length; i += 1 ) {
						data[param][i] = list[i].id;
					}
				} else { // IF SINGULAR OBJECT
					data[param] = object.relationships[param].data.id;
				}
			}
		});
	}

	return data;

};

// MIDDLEWARE

var that = this;

module.exports.middleware = function(request, response, next) {

	if ( request.body.data ) {

		request.body = that.deserialize(request.body);
		next();

	} else {
		next();
	}

};
