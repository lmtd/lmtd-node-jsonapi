var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');
var Errors = require('./../controllers/errors.js');

// EXPRESS
var express = require('express');
module.exports.express = express;

// APP
var app = express();
module.exports.app = app;

//
app.set('port', Config.APP.PORT);

// PROXY
app.enable('trust proxy');

// BODY PARSER
var bodyParser = require('body-parser');
module.exports.bodyParser = bodyParser;

// CORS
var cors = require('cors');
app.use(cors());

//
if ( Config.APP.BUGSNAG ) {
	app.use(Logger.bugsnag.requestHandler);
}

// ERROR HANDLER
app.use(function(error, request, responde, next) {
	Errors.BAD_REQUEST(responde, error);
});

if ( Config.APP.BUGSNAG ) {
	app.use(Logger.bugsnag.errorHandler);
}

// INIT
function backlog() {
	Logger.error( 'Express: Error (port: ' + Config.APP.PORT + ')' );
}
function callback() {
	Logger.log( 'Express: Success (port: ' + Config.APP.PORT + ')', true);
}

var server = require('http').Server(app);
server.listen( app.get('port'), backlog, callback);

module.exports.server = server;
