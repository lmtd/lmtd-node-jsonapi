var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');

var mongoose = require('mongoose');
module.exports.mongoose = mongoose;

mongoose.connect(Config.APP.MONGO.host + Config.APP.MONGO.database);

var db = mongoose.connection;
db.on('error', function(callback) {
	Logger.error('Mogoose: Error (database: ' + Config.APP.MONGO.database + ', host: ' + Config.APP.MONGO.host + ')');
});
db.once('open', function (callback) {
	Logger.log('Mongoose: Success (database: ' + Config.APP.MONGO.database.substring(0,40) + ', host: ' + Config.APP.MONGO.host.substring(0,40) + ')', true);
});
