var AWS = require('aws-sdk');
var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');

// SETUP -----------------------------------------------------------------------------------------------------

AWS.config.update(
	{
		apiVersions: '2006-03-01',
		accessKeyId: Config.APP.S3.accessKeyId,
		secretAccessKey: Config.APP.S3.secretAccessKey,
		region: Config.APP.S3.region,
	}
);

//

var S3 = new AWS.S3({ params: { Bucket: Config.APP.S3.bucket } });
module.exports.S3 = S3;

// CHECK IT CONNECTION AND BUCKET IS OKAY --------------------------------------------------------------------

function init() {
	S3.getBucketAcl({}, function(error, data) {
		if ( error ) {
			if ( error.code === 'NoSuchBucket') {
				var params = {
					Bucket: Config.APP.S3.bucket,
					ACL: 'public-read',
 					CreateBucketConfiguration: {
						LocationConstraint: Config.APP.S3.region
					}
  				};
  				S3.createBucket(params, function(error, data) {
  					if ( error ) {
  						Logger.error('Amazon S3: Bucket creation Error (bucket: ' + Config.APP.S3.bucket + ')');
  					} else {
  						Logger.log( 'Amazon S3: Bucket creation Success (bucket: ' + Config.APP.S3.bucket + ')', true);
  					}
				});
			} else if ( error.code === 'InvalidAccessKeyId' || error.code === 'ignatureDoesNotMatch' ) {
				Logger.error('Amazon S3: Authentication Error (bucket: ' + Config.APP.S3.bucket + ')');
			} else {
				Logger.error('Amazon S3: Error: ' + error.message);
			}
		} else {
			Logger.log( 'Amazon S3: Success (bucket: ' + Config.APP.S3.bucket + ')', true);
		}
	});
}

init();