var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');
var Errors = require('./../controllers/errors.js');

// INIT

var Intercom = require('intercom-client');
var client = new Intercom.Client(Config.APP.INTERCOM.app_id, Config.APP.INTERCOM.app_api_key);

// CHECK CONNECTION
client.tags.list(function(error, result) {
    if ( error ) {
        Logger.error('Intercom: Error: ' + error.statusMessage);
    } else {
        Logger.log('Intercom: Success');
    }
});

module.exports.client = client;
