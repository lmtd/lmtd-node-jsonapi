var Config = require('.././services/config.js');

Config.APP = JSON.parse(process.env.NODE_ENV);

module.exports.ENV = process.env.NODE_ENV || 'development';
