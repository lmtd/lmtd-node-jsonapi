var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');
var Express = require('.././initializers/express.js');

// ----------------------------------------------

var port = parseInt(Config.APP.PORT);

var io = require('socket.io')(Express.server, { path: Config.APP.SOCKET.path + '/socket.io' });

// LISTENER -------------------------------------

var listener = [];

module.exports.listeners = function(_listener) {
	listener.push(_listener);
};

// CONNECTION ----------------------------------

io.on('connection', function(socket) {

	// ADD LISTENERS
	for ( var i=0; i < listener.length; i += 1 ) {
		listener[i](socket);
	}

	// ADD INIT CONNECTED
	socket.emit('connected');

});

// INITIALIZE ----------------------------------

function backlog() {
	Logger.error( 'Socket.io: Error (port: ' + port + ')' );
}
function callback() {
	Logger.log( 'Socket.io: Success (port: ' + port + ', '+ 'path: ' + Config.APP.SOCKET.path + ')', true);
}

Express.server.listen(port, backlog, callback);

// PUBLIC API ----------------------------------

module.exports.io = io;
