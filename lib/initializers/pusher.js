var Config = require('.././services/config.js');
var Logger = require('.././services/logger.js');

var Pusher = require('pusher');

var pusher = new Pusher({
    appId: Config.APP.PUSHER.appId,
    key: Config.APP.PUSHER.key,
    secret: Config.APP.PUSHER.secret,
    encrypted: true
});
pusher.port = 443;

pusher.get({ path: '/channels/public', params: {} }, function(error, request, response) {
    if ( response ) {
        if ( response.statusCode === 200 ) {
            Logger.log('Pusher: Success');
        } else {
            Logger.error('Pusher: Error');
        }
    } else {
        Logger.error('Pusher: Error', error);
    }
});

module.exports = exports = pusher;
