var Logger = require('.././services/logger.js');
var IpApi = require('.././services/ipapi.js');

IpApi.getPromise('2.50.110.56').then(function(result) {

    Logger.log('IPAPI: Success');

}).catch(function(error) {

    Logger.error('IPAPI: Error', error);

});
