var fs = require('fs-extra');

module.exports.read = function(file) {
	return new Promise(function(resolve, reject) {
		fs.readFile(file, function (error, data) {
			if ( error ) {
				Logger.error(error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};

module.exports.remove = function(file) {
	return new Promise(function(resolve, reject) {
		fs.unlink(file, function (error, data) {
			if ( error ) {
				Logger.error(error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};

module.exports.copy = function(path, name) {
	return new Promise(function(resolve, reject) {
		fs.copy(path, name, function (error, data) {
			if ( error ) {
				Logger.error(error);
				reject(error);
			} else {
				resolve(name);
			}
		});
	});
};
