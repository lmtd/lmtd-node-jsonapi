var gm = require('gm');
var Logger = require('.././services/logger.js');

module.exports.resize = function(path, newfile, width) {
	return new Promise(function(resolve, reject) {
		gm(path).resize(width).write(newfile,function (error) {
			if (error) {
				Logger.error(error);
				reject(error);
			} else {
				resolve(true);
			}
		});
	});
};

module.exports.getSize = function(path) {
	return new Promise(function(resolve, reject) {
		gm(path)
		.size(function (error, data) {
			if (error) {
				Logger.error(error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};
