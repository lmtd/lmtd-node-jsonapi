var request = require('request-promise');
var Config = require('../services/config.js');
var Logger = require('../services/logger.js');

function getUrl(ip) {

    return 'http://pro.ip-api.com/json/'+ip+'?key=' + Config.APP.IP_API;

}

module.exports.getPromise = function(ip) {

    if ( ip === '127.0.0.1') { // BECUASE IT IS BEING TESTED LOCALLY
        ip = '92.253.88.228';
    }

	var url = getUrl(ip);

    return new Promise(function(resolve, reject) {

    	request({ uri:url, timeout: 1000, json:true }).then(function(result) {

            if ( result.status ) {
                if ( result.status === 'success' ) {
                    resolve(result);
                } else {
                    Logger.record('IPAPI Error:', result );
                    reject(result);
                }
            } else {
                Logger.record('IPAPI Error:', result );
                reject(result);
            }

    	}).catch(function(error) {

            Logger.record('IPAPI Error: Service is down', error);
            reject(error);

        });

    });

};
