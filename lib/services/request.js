var request = require('request');

//

module.exports.get = function(baseUrl, url, headers) {

    return this.factory('GET', baseUrl, url, headers);

};

module.exports.post = function(baseUrl, url, headers, data) {

    return this.factory('POST', baseUrl, url, headers, data);

};

//

module.exports.factory = function(method, baseUrl, url, headers, data) {

    var options = {
        method: method,
        url: baseUrl + '/' + url,
        headers: headers,
        json: true,
        rejectUnauthorized: false
    };

    if ( method === 'POST' ) {
        options.body = data;
    }

    return new Promise(function(resolve, reject) {
        request(options, function(error, response, body) {
            if ( error ) {
                reject(error);
            } else {
                if ( response.statusCode !== 200 ) {
                    reject(body);
                } else {
                    resolve(body);
                }
            }
        });
    });

};
