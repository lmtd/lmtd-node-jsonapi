var dns = require('dns');

//

module.exports.DOMAIN_NOT_FOUND = 'ENOTFOUND';

//

module.exports.getARecords = function(domain) {

    return new Promise(function(resolve, reject) {

        dns.lookup(domain,  { all:true }, function(error, addresses) {

            if ( error ) {
                reject(error);
            } else {
                if ( addresses.length > 0 ) {
                    resolve(addresses);
                } else {
                    reject({ code:'EMPTY' });
                }
            }

        });

    });

};

//

module.exports.getPTRRecords = function(domain) {

    return new Promise(function(resolve, reject) {

        dns.resolve(domain, rrtype='PTR', function(error, addresses) {

            if ( error ) {
                reject(error);
            } else {
                if ( addresses.length > 0 ) {
                    resolve(addresses);
                } else {
                    reject({ code:'EMPTY' });
                }
            }

        });

    });

};
