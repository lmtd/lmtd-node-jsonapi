var Config = require('./../services/config.js');
var bugsnag = require('bugsnag');

console.log(' ');

// PUBLIC  ---------------------------------------------------------------------

module.exports.log = function(message) {

	console.log(message);

};

module.exports.record = function(message, object) {

	if ( !object ) {
		object = {};
	}

	console.log(message, object);

	if ( Config.APP.BUGSNAG ) {
		bugsnag.notify(new Error(message), { severity: 'info', object: object });
	}

};

module.exports.error = function(message) {

	console.error(message);

	if ( Config.APP.BUGSNAG ) {
		bugsnag.notify( message );
	}

};

// INIT BUGSNAG ----------------------------------------------------------------

module.exports.bugsnag = bugsnag;

var hasBugsnagInitiated = false;

if ( Config.APP ) {
	if ( Config.APP.BUGSNAG ) {
		if ( !hasBugsnagInitiated ) {

			bugsnag.register(Config.APP.BUGSNAG);
			hasBugsnagInitiated = true;

			this.log('Bugsnag: Success');

			var self = this;
			process.on('unhandledRejection', function(reason, promise) {
				self.error('Unhandled Rejection', { reason: reason, promise: promise });
			});

		}
	}
}
