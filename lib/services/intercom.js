var Intercom = require('.././initializers/intercom').client;
var Utils = require('.././services/utils');
var Logger = require('.././services/logger.js');

module.exports.event = function(email, name, metadata) {

    return new Promise(function(resolve, reject) {

        var data = { event_name: name, email: email, created_at: Utils.time() };
        if ( metadata ) { data.metadata = metadata; }

        Intercom.events.create(data, function(error, result) {
            if ( error ) {
                Logger.record('Intercom: Error:', error);
                reject(error);
            } else {
                if ( result.ok ) {
                    resolve({});
                } else {
                    Logger.record('Intercom: Error: unknown error', result);
                    reject({ error: 'unknown' });
                }
            }
        });

    });

};

module.exports.delete = function(user) {

    return new Promise(function(resolve, reject) {

        Intercom.users.delete({ user_id: user.id }, function(error, result) {
            if ( error ) {
                Logger.record('Intercom: Error:', error);
                reject(error);
            } else {
                if ( result.ok ) {
                    resolve(console.body);
                } else {
                    Logger.record('Intercom: Error: unknown error', result);
                    reject({ error: 'unknown' });
                }
            }
        });

    });

};

module.exports.update = function(user) {

    return new Promise(function(resolve) {

        resolve({});

    });

};
