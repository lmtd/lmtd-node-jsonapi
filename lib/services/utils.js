function randoms(length, chars) {
    var result = '';
    for ( var i=0; i < length; i += 1 ) {
        result += chars[ Math.round(Math.random() * (chars.length - 1)) ];
    }
    return result;
}


// RANDOM ----------------------------------------------------------------------

module.exports.randomcombined = function(length) {

    var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    return randoms(length, chars);

};

module.exports.randomstring = function(length) {

    var chars = 'abcdefghijklmnopqrstuvwxyz';
    return randoms(length, chars);

};

module.exports.randomnumber = function(length) {

    var chars = '0123456789';
    return randoms(length, chars);

};

module.exports.isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

//

module.exports.time = function() {
    var unix = Math.round(+new Date()/1000);
    return unix;
};

module.exports.replace = function(source, what, to) {
    return source.split(what).join(to);
};

module.exports.numberLength = function(source) {
    var numbers = ['0','1','2','3','4','5','6','7','8','9'];
    var length = 0;
    for ( var i=0; i < source.length; i += 1 ) {
        if ( numbers.indexOf(source[i]) !== -1 ) {
            length += 1;
        }
    }
    return length;
};
