// IMPORTS

var initializer = require('.././initializers/socket.js');
var io = initializer.io;

// LISTENER ----------------------------------------------------

module.exports.listeners = function(listener) {
	initializer.listeners(listener);
};

// PUBLIC BASE API ---------------------------------------------

module.exports.rooms = function() {
	return io.sockets.adapter.rooms;
};

// ROOM RELATED API ---------------------------------------------

module.exports.put = function(data, room) {
	if ( room ) {
		io.to(room).emit('put', data);
	} else {
		io.emit('put', data);
	}
};

module.exports.delete = function(data, room) {
	if ( room ) {
		io.to(room).emit('delete', data);
	} else {
		io.emit('delete', data);
	}
};

module.exports.post = function(data, room) {
	if ( room ) {
		io.to(room).emit('post', data);
	} else {
		io.emit('post', data);
	}
};
