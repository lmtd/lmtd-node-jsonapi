var S3 = require('.././initializers/amazons3.js').S3;
var FileSystem = require('.././services/filesystem.js');
var Logger = require('.././services/logger.js');

// SAVE FILE FROM DISK ----------------------------------------------------------------------------

module.exports.save = function(file, key, ispublic, mimetype) {
	return new Promise(function(resolve, reject) {
		var params = { Body: file, Key: key, ContentType: mimetype };
		if ( ispublic !== false ) {
			params.ACL = 'public-read';
		}
		S3.upload(params, function(error, data) {
			if ( error ) {
				Logger.record('Amazon Error', error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};

// REMOVE OBJECT ------------------------------------------------------------------------------------

module.exports.remove = function(filename) {
	return new Promise(function(resolve, reject) {
		var params = { Key: filename };
		S3.deleteObject( params, function(error, data) {
			if ( error ) {
				Logger.record('Amazon Error', error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};

module.exports.removeList = function(list) {
	return new Promise(function(resolve, reject) {
		var params = [];
		for ( var i=0; i < list.length; i += 1 ) {
			params.push({ Key: list[i] });
		}
		S3.deleteObjects( { Delete: { Objects: params } } , function(error, data) {
			if ( error ) {
				Logger.record('Amazon Error', error);
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
};

// TRANSFER -----------------------------------------------------------------------------------------

module.exports.transfer = function(path, key, mimetype) {

	var that = this;

	return FileSystem.read(path).then(function(file) {

		return that.save(file, key, true, mimetype).then(function() {

			return FileSystem.remove(path);

		});

	});

};
