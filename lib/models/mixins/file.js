module.exports = exports = function(schema) {

	schema.set('toObject', {
		getters: true,
		virtuals: false,
		versionKey: false,
		transform: function (doc, ret) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			delete ret.originalname;
			delete ret.mimetype;
			delete ret.size;
		}
	});

	//

	schema.add({

		mimetype: { type: String, required: true },

		originalname: { type: String, required: true },

		size: { type: Number, required: true },

		filename: { type: String, get: function() { return this._id + '.' + this.extension; } }

	});

	//

	schema.virtual('extension').get(function () {
		if ( this.originalname ) {
			if ( this.originalname.indexOf('.') ) {
				var array = this.originalname.split('.');
				return array[array.length-1];
			} else { 
				var array = this.mimetype.split('/');
				return array[array.length-1]; 
			}
		} else {
			return '';
		}  
	});

};