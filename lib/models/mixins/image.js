var sizes = [160, 320, 480, 640, 960, 1280, 1920, 2560];

function calculateFilename(id, width, extension) {
	return id + '_' + width +'.' + extension;
}

module.exports = exports = function(schema) {

	schema.add({

		sizes: { type: Number, required: true },

		width: { type: Number, required: true },

		height: { type: Number, required: true },

	});

	//

	schema.statics.calculateSize = function(width) {
		for ( var i=0; i < sizes.length; i += 1 ) {
			var size = sizes[i];
			if ( size > width ) {
				return i;
			}
		}
	};

	//

	schema.statics.calculateFilename = function(id, width, extension) {
		return calculateFilename(id, width, extension);
	};

	//

	schema.virtual('list').get(function () {
		var list = [];
		for ( var i=0; i < this.sizes; i += 1 ) {
			list.push( calculateFilename(this.id, sizes[i], this.extension) );
		}
		return list;
	});

};
