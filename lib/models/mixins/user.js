var crypto = require('crypto');

module.exports = exports = function(schema) {

	//

	schema.add ( {

		email : { type : String, required : true, index : { unique : true }},
		hash : { type : String, required : true },
		key : { type : String, required : true },

		__v: { type: Number, select: false }

	});

	//

	schema.set('toJSON', {
		transform: function (doc, ret) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			delete ret.hash;
			delete ret.key;
		}
	});

	//

	schema.set('toObject', {
		getters: true,
		virtuals: false,
		versionKey: false,
		transform: function (doc, ret) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			delete ret.hash;
			delete ret.key;
		}
	});

	// VIRTUALS

	schema.virtual('password').set(function(password) {
		if ( password ) {
			if ( this.authenticate(password) === false ) {
				this.key = Math.round(new Date().valueOf() * Math.random()) + '';
	    		this.hash = this.encrypt(password);
			}
		}
	});

	schema.virtual('token').get(function () {
    	return this.hash;
	});

	// INTERNALS

	schema.methods.encrypt = function (plaintext) {
		return crypto.createHmac('sha256', this.key).update(plaintext).digest('hex');
	};

	// EXTERNALS

	schema.methods.authenticate = function(plaintext) {
		if ( plaintext ) {
			if ( plaintext && this.hash ) {
	    		return this.encrypt(plaintext) === this.token;
	    	} else {
	    		return false;
	    	}
		} else {
			return false;
		}
	};

};
