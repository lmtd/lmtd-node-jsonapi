var S3 = require('../.././services/amazons3.js');
var Logger = require('../.././services/logger.js');

module.exports = exports = function(schema) {

	schema.post('remove', function(document) {

		var filename = document.toObject().filename;

  		S3.remove( filename ).then(function(data) {

  		}).catch(function(error) {
  			Logger.error('Amazon S3 Error:', { message:'Could not remove file', error: error });
  		});

	});

};
