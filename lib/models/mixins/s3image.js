var S3 = require('../.././services/amazons3.js');
var Logger = require('../.././services/logger.js');

module.exports = exports = function(schema) {

	schema.post('remove', function(document) {

		var list = document.list; // SIZES
		list.push(document.toObject().filename); // ORIGINAL

  		S3.removeList( list ).then(function(data) {

  		}).catch(function(error) {
  			Logger.error('Amazon S3 Error:', { message:'Could not remove image', error: error });
  		});
	});

};
