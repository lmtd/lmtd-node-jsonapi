var Logger = require('../.././services/logger.js');

module.exports = exports = function(schema) {

	// METHODS ----------------------------------------------------------------------

	schema.methods.promiseSave = function() {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.save(function(error, data) {
				if ( error ) {
					Logger.error(error);
					reject(error);
				} else {
					resolve(that);
				}
			});
		});
	};

	// -----

	schema.methods.promiseRemove = function() {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.remove(function(error, data) {
				if ( error ) {
					Logger.error(error);
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	};

	// -----

	schema.methods.update = function(object) {

		var self = this;

		return new Promise(function(resolve, reject) {

			var list;
			if ( self.preUpdate ) {
				list = self.preUpdate(object);
			}

			// UPDATE PARAMS
			Object.keys(object).forEach(function(param) {
				if ( param !== '_id' && param !== 'type' ) {
					if ( object[param] !== "undefined" && object[param] !== null && object[param] !== "null" ) {
						self[param] = object[param];
					}
				}
			});

			if ( self.postUpdate ) {
				self.postUpdate(list).then(function(result) {
					resolve(result);
				}).catch(function(error) {
					Logger.error(error);
					reject(error);
				});
			} else {
				self.promiseSave().then(function(result) {
					resolve(result);
				}).catch(function(error) {
					Logger.error(error);
					reject(error);
				});
			}

		});

	};

	// STATICS --------------------------------------------------------------------

	schema.statics.getList = function(query, fields, operation) {
		if ( !query ) {
			query = {};
		}
		if ( !operation ) {
			operation = {};
		}
		var that = this;
		return new Promise(function(resolve, reject) {
			that.find(query, fields, operation, function(error, data) {
				if ( error ) {
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	};

	// -----

	schema.statics.getOneById = function(id, fields) {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.find({ _id:id }, fields, function(error, data) {
				if ( error ) {
					reject(error);
				} else if ( data.length !== 0 ) {
					var document = data[0];
					resolve(document);
				} else {
					error = {
						message: 'Could not find',
						id: id,
						type: schema.tree.type.get(),
					};
					//Logger.record(error);
					reject(error);
				}
			});
		});
	};

	// -----

	schema.statics.getOneByParameter = function(params) {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.find(params, function(error, data) {
				if ( error ) {
					reject(error);
				} else if ( data.length !== 0 ) {
					var document = data[0];
					resolve(document);
				} else {
					reject(false);
				}
			});
		});
	};

	// -----

	schema.statics.removeById = function(id) {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.getOneById(id).then(function(document) {
				document.promiseRemove().then(function(result) {
					resolve(result);
				}).catch(function(error) {
					Logger.error(error);
					reject(error);
				});
			}).catch(function(error) {
				reject(error);
			});
		});
	};

	// -----

	schema.statics.promiseCount = function() {
		var that = this;
		return new Promise(function(resolve, reject) {
			that.count(function(error, data) {
				if ( error ) {
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	};

};
