'use strict';

function registerQuery(queries, modelName, id) {

	if ( queries[modelName] ) {
		if ( queries[modelName].indexOf(id) === -1 ) {
			queries[modelName].push(id);
		}
	} else {
		queries[modelName] = [ id ];
	}

}

//

module.exports = exports = function(schema) {

	// SERIALIZE ------------------------------------------------------------------------

	schema.methods.serialize = function() {

		const object = this.toObject();

		const data = {};
		data.attributes = {};
		data.relationships = {};
		data.id = object.id;
		data.type = object.type;

		delete object.id;
		delete object.type;

		Object.keys(object).forEach(function(param) {

			if ( schema.paths[param] ) { // IGNORE THE ONES NOT DEFINED IN THE SCHEMA

				const type = schema.paths[param].instance;

				if ( type === 'Array' ) { // IF RELATIONSHIPS

						data.relationships[param] = { data: [] };
						const list = object[param];

						for ( let i=0; i < list.length; i += 1 ) {
							data.relationships[param].data[i] = {
								id: list[i],
								type: schema.paths[param].options.type[0].ref
							};
						}

				} else if ( type === 'ObjectID' ) { // IF SINGULAR OBJECT

					data.relationships[param] = {
						data : {
							id: object[param],
							type: schema.paths[param].options.ref
						}
					};

				} else { // IF ATTRIBUTE

					data.attributes[param] = object[param];

				}

			}

		});

		// REMOVE PLACEHOLDER IS EMPTY
		if ( Object.keys(data.attributes).length === 0 ) { delete data.attributes; }
		if ( Object.keys(data.relationships).length === 0 ) { delete data.relationships; }

		return data;

	};

	// INCLUDED ----------------------------------------------------------------------------

	schema.methods.getIncludedQueries = function(queries, included) {

		const data = this.serialize();

		if ( data.relationships ) {

			Object.keys(data.relationships).forEach(function(param) {

				if ( included.indexOf(param) !== -1 ) {

					if ( schema.paths[param].options.type.schemaName === 'ObjectId' ) { // IF SINGULAR

						const item = data.relationships[param].data;

						registerQuery(queries, item.type, item.id);

					} else if ( schema.paths[param].options.type[0] ) { // IF ARRAY

						const list = data.relationships[param].data;
						for ( let i=0; i < list.length; i += 1 ) {

							const item = list[i];
							registerQuery(queries, item.type, item.id);

						}

					}

				}

			});

			return queries;

		} else {
			return {};
		}

	};


};
