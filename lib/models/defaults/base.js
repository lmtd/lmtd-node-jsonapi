module.exports = exports = function(schema) {

	schema.set('toObject', {
		getters: true,
		virtuals: false,
		versionKey: false,
		transform: function (doc, ret) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
		}
	});

};