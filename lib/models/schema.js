var mongoose = require('.././initializers/mongoose').mongoose;
var Schema = mongoose.Schema;
// DEFAULTS
var Serializer = require('./defaults/serializer.js');
var Methods = require('./defaults/methods.js');
var Base = require('./defaults/base.js');
// UTILS
var ExtendedValidator = require('./utils/extendedvalidator.js');
var EmbeddedRemover = require('./utils/embeddedremover.js');
var SaveRemover = require('./utils/saveremover.js');
var DependencyRemover = require('./utils/dependencyremover.js');

module.exports.Types = Schema.Types;

//

module.exports.getModel = function(type) {
	if ( type ) {
		return mongoose.model(type);
	} else {
		return null;
	}
};

// EMBEDDED MAP ------------------------------------------------------------------------------

var dependencymap = {};

module.exports.getDependency = function(type) {

	if ( dependencymap[type] ) {

		var object = dependencymap[type];

		if ( object.length ) {
			var array = [];
			for ( var i=0; i < object.length; i += 1 ) {
				array.push( { model: mongoose.model(object[i].model), parameter: object[i].parameter } );
			}
			return array;
		} else {
			return { model: mongoose.model(object.model), parameter: object.parameter };
		}

	} else {
		return null;
	}

};

// SCHEMA CREATION -------------------------------------------------------------------------

var registered = [];

module.exports.Schema = function(type, declarations, plugins, dependency) {

	registered.push(type);

	//
	declarations.type = { type: String, get: function() { return type } };

	// DEFAULT VALIDATORS ---------------------------

	Object.keys(declarations).forEach(function(param) {
		if ( declarations[param].type ) {
			if ( declarations[param].type.toString().indexOf('String') !== -1 && param !== 'type' ) { // IF STRING
				if ( !declarations[param].maxlength ) {
					declarations[param].maxlength = 1024;
				}
				if ( declarations[param].items ) {
					declarations[param].enum = {
						values: declarations[param].items,
						message: 'enum validator failed for path {PATH} with value {VALUE}'
					};
					declarations[param].lowercase = true;
					delete declarations[param].items;
				}
			}
		}
	});

	//-----------------------------------------------

	//
	var Instance = new Schema(declarations);

	// THE VERY BASICS
	Instance.plugin(Serializer);
	Instance.plugin(Methods);
	Instance.plugin(ExtendedValidator);
	Instance.plugin(Base);

	// CUSTOM PLUGINS
	if ( plugins ) {
		for ( var i=0; i < plugins.length; i += 1 ) {
			Instance.plugin(plugins[i]);
		}
	}

	// AUTO DETECT
	var embeddedremove = false;
	var saveremove = false;
	Instance.eachPath(function(path, schemaType) {
		if ( schemaType.options.type[0] ) {
			if ( schemaType.options.type[0].embedded ) {
				embeddedremove = true;
			}
		}
		if ( schemaType.options.embedded ) {
			embeddedremove = true;
		}
		if ( schemaType.options.saveremove ) {
			saveremove = true;
		}
	});
	if ( embeddedremove ) {
		Instance.plugin(EmbeddedRemover);
	}
	if ( saveremove ) {
		Instance.plugin(SaveRemover);
	}

	// DEPENDENCY REMOVER
	if ( dependency ) {
		dependencymap[type] = dependency;
		Instance.plugin(DependencyRemover);
	}

	//
	return mongoose.model(Instance.tree.type.get(), Instance);

};
