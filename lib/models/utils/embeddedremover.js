var mongoose = require('mongoose');
var Logger = require('../.././services/logger.js');

module.exports = exports = function(schema) {

	schema.post('remove', function(document) {

		var self = this;
		var postremove = [];

		schema.eachPath(function (path, schemaType) {
			if ( schemaType.instance === 'ObjectID' && path !== '_id' ) {
				if ( self[path] && schemaType.options.embedded ) {
					if ( schemaType.options.ref ) {
						var Model = mongoose.model(schemaType.options.ref);
						postremove.push( Model.removeById( self[path]) );
					}
				}
			}
			if ( schemaType.instance === 'Array' ) {
				if ( self[path] && schemaType.options.type[0].embedded ) {
					if ( schemaType.options.type[0].ref ) {
						var Model = mongoose.model(schemaType.options.type[0].ref);
						var list = self[path];
						for ( var i=0; i < list.length; i += 1 ) {
							postremove.push( Model.removeById( list[i]) );
						}
					}
				}
			}
		});
		if ( postremove.length > 0 ) {
				Promise.all(postremove).then(function() {
			}).catch(function(error) {
				Logger.error(error);
			});
		}

	});

};
