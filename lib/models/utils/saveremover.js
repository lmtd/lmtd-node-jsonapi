var mongoose = require('mongoose');

module.exports = exports = function(schema) {

	schema.methods.preUpdate = function(object) {
			var self = this;
			var postupdate = []; 
			schema.eachPath(function (path, schemaType) {
				if ( schemaType.instance === 'ObjectID' && path !== '_id' ) {
					if ( self[path] && object[path] ) {
						if ( schemaType.options.saveremove ) {
							if ( self[path].toString() !== object[path].toString() ) {
								var Model = mongoose.model(schemaType.options.ref);
								var promise = Model.removeById(self[path]);
								postupdate.push( promise );
							}
						}
					}
				}
			});
			return postupdate;

	};

	schema.methods.postUpdate = function(postupdate) {
		var self = this;
		return new Promise(function(resolve, reject) {
			self.save(function(error, data) {
				if ( error ) {
					reject(error);
				} else {
					if ( postupdate.length > 0 ) {
						Promise.all(postupdate).then(function(data) {
							resolve(data);
						}).catch(function(error) {
							if ( !error ) {
								resolve(true);
							} else {
								reject(error);
							}
						});
					} else {
						resolve(data);
					}
				}
			});

		});
	};

};