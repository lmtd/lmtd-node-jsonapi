var Logger = require('../.././services/logger.js');
var MySchema = require('.././schema.js');

module.exports = exports = function(schema) {

	schema.pre('remove', function(next) {

		var dependency = MySchema.getDependency(this.type);

		if ( dependency ) {

			if ( dependency.length ) {

				for ( var i=0; i < dependency.length; i += 1 ) {
					remove(dependency[i].model, dependency[i].parameter, String(this._id));
				}

			} else {
				remove(dependency.model, dependency.parameter, String(this._id));
			}

		}

		next();

	});

};

function remove(Model, Parameter, actual) {

	var query = {};
	query[Parameter] = actual;

	Model.getList(query).then(function(document) {

		var relatedschema = Model.schema;
		var promises = [];
		if ( document.length ) {
			for ( var i=0; i < document.length; i += 1 ) {
				promises.push( findAndUpdate(Model, relatedschema, document[i]._id, actual, Parameter ) );
			}
		}
		if ( promises.length === 0 ) { // IT's OKAY, it may have been removed by embeddedremover
			//Logger.record("dependencyremover.js: couldn't find any object with related reference");
		} else {
			Promise.all(promises).then(function() {

			});
		}
	});
	//.catch(function(error) {
		//Logger.log(error);
	//});

}

function findAndUpdate(Model, relatedschema, id, actual, Parameter) {
	return Model.getOneById(id).then(function(document) {
		if ( relatedschema.tree[Parameter].type ) {
			document[Parameter] = undefined;
		} else {
			var list = document[Parameter];
			var newList = [];
			for ( var n=0; n < list.length; n += 1 ) {
				if ( list[n].toString() !== actual.toString() ) {
					newList.push(list[n]);
				}
			}
			document[Parameter] = newList;
		}
		return document.promiseSave();
	});
}
