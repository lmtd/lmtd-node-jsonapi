var ExpressInitializer = require('./../initializers/express.js');
var Config = require('./../services/config.js');
var Errors = require('./../controllers/errors.js');
//
var Route = require('./../controllers/route.js');
var router = Route.init('/upload', false);

if ( Config.APP.MAX_UPLOAD_SIZE ) {
	router.use(ExpressInitializer.bodyParser.urlencoded({ extended: true, limit: Config.MAX_UPLOAD_SIZE }));
}

//

var multer = require('multer');
var path = require('path');
var crypto = require('crypto');

// FILE ------------------------------------------------------------------------

module.exports.file = function(authentication) {

    set('/file', { dest: './files' }, authentication, true);

};

// SET -------------------------------------------------------------------------

module.exports.set = function(path, options, authentication, single) {

    set(path, options, authentication, single);

};

//

function set(path, options, authentication, single, after) {

    router.route(path).post(function(request, response) {

        authentication(null, request, response).then(function(user) {

            getPromise(options, single, request, response).then(function(result) {

                if ( options.after ) {

                    options.after(result, request).then(function(result) {

                        response.json(result);

                    }).catch(function(error) {

                        Errors.INTERNAL_SERVER_ERROR(response, error);

                    });
                } else {
                    response.json(result);
                }

			}).catch(function(error) {
	            Errors.INTERNAL_SERVER_ERROR(response, error);
	        });

        }).catch(function(error) {
            Errors.UNAUTHORIZED(response, error);
        });

    });

}

function getPromise(options, single, request, response) {

    var storage = getStorage(options);

    var upload;
    if ( single ) {
        upload = multer({storage: storage}).single('file');
    } else {
        upload = multer({storage: storage}).array('files');
    }

    return new Promise(function(resolve, reject) {

        upload(request, response, function(error) {

            if ( error ) {
                reject(error);
            } else {
                if ( single ) {
                    resolve(request.file);
                } else {
                    resolve(request.files);
                }
            }

        });

    });

}

function getStorage(options) {

    if ( !options.dest ) {
        options.dest = './temp/upload/';
    }

    var storage = multer.diskStorage({

        destination: function (req, file, cb) {
            cb(null, options.dest);
        },

        filename: function (req, file, cb) {
            var id = crypto.randomBytes(20).toString('hex');
            cb(null, id + path.extname(file.originalname) );
        }

    });

    return storage;

}
