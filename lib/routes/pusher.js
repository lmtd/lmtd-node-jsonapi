var Express = require('./../initializers/express.js');
var Pusher = require('./../initializers/pusher.js');
var Authenticator = require('./../controllers/authenticator.js');

var Route = require('./../controllers/route.js');

// SEND

module.exports.send = function(request, response, user) {

    if ( request.body.socket_id && request.body.channel_name ) {

        var socketId = request.body.socket_id;
        var channel = request.body.channel_name;

        var presenceData = {
            id: user.id,
            email: user.email
        };

        var auth = Pusher.authenticate(socketId, channel, presenceData);
        response.send(auth);

    } else {
        response.sendStatus(403);
    }

};

// INIT

var that = this;

module.exports.init = function(Model, method) {

    var router = Route.init('/pusher');

    router.use(Express.bodyParser.json());
    router.use(Express.bodyParser.urlencoded({ extended: false }));

    router.route('/auth').post(function(request, response) {

        if ( !method ) {

            Authenticator.authentication(null, request, response, Model).then(function(user) {

                that.send(request, response, user);

            }).catch(function() {

                response.sendStatus(403);

            });

        } else {

            method(request, response);

        }

    });

    return router;

};
