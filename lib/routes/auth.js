var Errors = require('./../controllers/errors.js');
var Authenticator = require('./../controllers/authenticator.js');
var Logger = require('.././services/logger.js');
var Utils = require('.././services/utils.js');
var Route = require('.././controllers/route.js');

module.exports.init = function(Model, middlewares) {

	if ( !middlewares ) {
		middlewares = {};
	}

	var router = Route.init('/auth', true);

	// LOGIN -------------------------------

	router.route('/login').post( function(request, response) {

		Authenticator.login(request, response, Model).then(function(result) {
			response.json(result);
		}).catch(function(error) {
			if ( error === 'password' || error === 'email' ) {
				Errors.UNAUTHORIZED(response, error);
			} else {
				Errors.BAD_REQUEST(response, null);
			}
		});

	});

	// CHANGE PASSWORD ----------------------------

	router.route('/changepassword').post(function(request, response) {

		Authenticator.authentication(null, request, response, Model).then(function(user) {

			if ( request.body.password ) {

				var password = request.body.password;

				user.password = password;
				user.promiseSave().then(function() {

					if ( middlewares.changepassword ) {

						middlewares.changepassword(user, password).then(function() {
							response.json({ token: user.token });
						}).catch(function(error) {
							Errors.INTERNAL_SERVER_ERROR(response, error);
						});

					} else {
						response.json({ token: user.token });
					}

				}).catch(function(error) {
					Errors.INTERNAL_SERVER_ERROR(response, error);
				});

			} else {
				Errors.BAD_REQUEST(response, null);
			}

		}).catch(function(error) {
			if ( error === 'password' || error === 'email' ) {
				Errors.UNAUTHORIZED(response, error);
			} else {
				Errors.BAD_REQUEST(response, null);
			}
		});

	});

	// RESET PASSWORD ----------------------------

	router.route('/resetpassword').post(function(request, response) {

		if ( request.body.email ) {

			Model.getOneByParameter({email: request.body.email}).then(function(user) {

				var password = Utils.randomstring(8);
				user.password = password;

				user.promiseSave().then(function() {

					if ( middlewares.resetpassword ) {

						middlewares.resetpassword(user, password).then(function() {
							response.json({ message: 'success' });
						}).catch(function(error) {
							Errors.INTERNAL_SERVER_ERROR(response, error);
						});

					} else {
						response.json({ message: 'success' });
					}

				}).catch(function(error) {
					Errors.INTERNAL_SERVER_ERROR(response, error);
				});

			}).catch(function() {
				Errors.UNAUTHORIZED(response, 'email');
			});

		} else {
			Errors.BAD_REQUEST(response, null);
		}

	});

	// SETUP -------------------------------------

	Model.count( function(error, count) {
		if ( count < 1 ) {
			var document = new Model({ email: 'test@test.com', password: 'test@test.com' });
			document.promiseSave().then(function() {
				Logger.log('Authentication: test@test.com added', true);
			}).then(function(error) {

			});
		}
	});

	// RETURN -------------------------------

	return router;

};
